var flag; //add与update标识
var xianJqGridRowData; //点击行数据   
var importantPlaceJqGridRowData;
//chosen-select 插件初始化
$(".chosen-select").chosen();
$.jgrid.defaults.styleUI = 'Bootstrap';

var mydata;
var totalRecords;
//var serachText; // 模糊查询
var mydataSerach;
var totalRecordsSerach;
var flagSerach;
var totalPage;

$.jgrid.defaults.styleUI = 'Bootstrap';


//人员组织架构
$.ajax({
	type: "get",
	url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/getOrgaInfo",

	dataType: "json",
	async: false,
	headers: {
		"Content-Type": "application/json;charset=UTF-8"
	},
	success: function(data) {
		//			console.log(data);
		for(var i = 0; i < data.length; i++) {
			var ele = "<option value='" + data[i].id + "'>" + data[i].name + "</option>";
			$("#stationid").append(ele);
			$("#stationidstationid").append(ele);
		}
		$(".chosen-select").trigger("chosen:updated");
	}
});

$.ajax({
	type: "get",
	url: YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/MapLine/",
	data: {
		"pageIndex": 0,
		"pageSize": 10
	},
	dataType: "json",
	async: false,
	headers: {
		"Content-Type": "application/json;charset=UTF-8"
	},
	success: function(data) {
		mydata = data.data;
		totalRecords = data.elementsSum;
		totalPage = countTotalPage(totalRecords);
	}
});

function countTotalPage(totalRecords) {
	var pages;
	if(totalRecords <= 10) {
		pages = 1;
	} else {
		if(totalRecords % 10 == 0) {
			pages = totalRecords / 10;
		} else {
			pages = parseInt(totalRecords / 10) + 1;
		}
	}
	return pages;
}
//表格初始	
$("#table_list_1").jqGrid({
	data: mydata,
	datatype: "local",
	height: 530,
	autoScroll: true,
	autowidth: true,
	shrinkToFit: true,
	rowNum: 10,
	
	colNames: ['资源类型编码','id', '线对象编码', '线名称', '等级', '备注', '所属部门', '所属部门id'],
		colModel: [
		    {
				name: 'resourceTypeCode',
				index: 'resourceTypeCode',
				width: 100

			},
			{
				name: 'id',
				index: 'id',
				hidden: true
			},
			{
				name: 'linecode',
				index: 'linecode',
                width: 140
			},
			{
				name: 'name',
				index: 'name',
                width: 220
			},
			{
				name: 'objlevel',
				index: 'objlevel',
				width: 80

			},
			{
				name: 'remark',
				index: 'remark',
                width: 200
			},
			{
				name: 'stationName',
				index: 'stationName',
				width: 140

			},
			{
				name: 'stationid',
				index: 'stationid',
				width: 80,
                hidden:true
			}
		],
	
	viewrecords: true,
	caption: "",
	hidegrid: false,
	onSelectRow: function(rowId, status) {
		xianJqGridRowData = $("#table_list_1").jqGrid("getRowData", rowId);
		//		console.log(cameraJqGridRowData);
	}

});

$.jqPaginator('#pagination1', {
	totalPages: totalPage,
	visiblePages: 10,
	currentPage: 1,
	onPageChange: function(num, type) {

		var mydata;
		$.ajax({
			type: "get",
			url: YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/MapLine/",
			data: {
				"pageIndex": num - 1,
				"pageSize": 10,

			},
			dataType: "json",
			async: false,
			headers: {
				"Content-Type": "application/json;charset=UTF-8"
			},
			success: function(data) {
				//	console.log(data);
				mydata = data.data;
				totalRecords = data.elementsSum;
			}
		});

		$("#table_list_1").jqGrid('clearGridData'); //清空表格
		$("#table_list_1").jqGrid('setGridParam', { // 重新加载数据
			datatype: 'local',
			data: mydata, //  newdata 是符合格式要求的需要重新加载的数据 

		}).trigger("reloadGrid");
	}
});

function initGrid() {
	$.ajax({
		type: "get",
		url: YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/MapLine/",
		dataType: "json",
		async: false,
		headers: {
			"Content-Type": "application/json;charset=UTF-8"
		},
		success: function(data) {
			//console.log(data);
			mydata = data;
			var length = mydata.length;
			var listArray = [];
			for(var i = 0; i < length; i++) {
				listArray.push(mydata[i]['stationid']);
			}
			//console.log("listArray"+JSON.stringify(listArray));
			$.ajax({
				type: "post",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/byIdList",
				dataType: "json",
				data: JSON.stringify(listArray),
				async: false,
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					var lengthOne = data.length;
					for(var j = 0; j < lengthOne; j++) {
						mydata[j]['stationName'] = data[j]['stationName'];
					}
					//console.log("data"+JSON.stringify(data));
					//console.log("mydata"+JSON.stringify(mydata));

				},
				error: function(returndata) {
					console.log(returndata);

				}

			});

		}
	});

	var jqGriddata;
	$.ajax({
		type: "get",
		url: YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/resource/placeInfoCRUD/queryAllIM",
		dataType: "json",
		async: false,
		headers: {
			"Content-Type": "application/json;charset=UTF-8"
		},
		success: function(data) {
			jqGriddata = data;
		},
		error: function(returndata) {
			console.log(returndata);
		}

	});

	$("#table_list_2").jqGrid({
		data: jqGriddata,
		datatype: "local",
		//autoScroll: true,
		autowidth: true,
		shrinkToFit: true,
		height: 350,
		colNames: ['id', '地址', '大图', '编码', '描述', '资源名称', '场所类别', '小图', '部门id', '部门名称', '纬度', '经度', '点位名称', '扩展属性表', '资源类型编码'],
		colModel: [{
				name: 'id',
				index: 'id',
				hidden: true
			},
			{
				name: 'address',
				index: 'address',
				width: 200

			},

			{
				name: 'bimage',
				index: 'bimage',
				hidden: true

			},
			{
				name: 'code',
				index: 'code',

			},

			{
				name: 'description',
				index: 'description',
				width: 200

			},
			{
				name: 'name',
				index: 'name',
				width: 200

			},
			{
				name: 'placetype',
				index: 'placetype',
				hidden: true

			},
			{
				name: 'simage',
				index: 'simage',
				hidden: true

			},
			{
				name: 'stationid',
				index: 'stationid',
				hidden: true

			},
			{
				name: 'stationName',
				index: 'stationName',
				hidden: true

			},
			{
				name: 'latitude',
				index: 'latitude',
				hidden: true

			},
			{
				name: 'longitude',
				index: 'longitude',
				hidden: true

			},
			{
				name: 'pointName',
				index: 'pointName',
				width: 200

			},
			{
				name: 'owner',
				index: 'owner',
				hidden: true

			},
			{
				name: 'resourceTypeCode',
				index: 'resourceTypeCode',
				hidden: true

			}

		],
		viewrecords: true,
		rowNum: 10,
		pager: "#pager_list_2",
		onSelectRow: function(rowId, status) {
			importantPlaceJqGridRowData = $("#table_list_2").jqGrid("getRowData", rowId);

		},
	});
}
initGrid();
// Add responsive to jqGrid
$(window).bind('resize', function() {
	var width = $('.jqGrid_wrapper').width();
	$('#table_list_1').setGridWidth(width);

});

$("#add-btn").click(function() {
	flag = "add";
	$("#commentForm").find("input").val("");
	$("#resourceTypeCode").val('巡控线路');
	$("#stationid option").eq(0).prop("selected", true);
	$(".chosen-select").trigger("chosen:updated");
	$(".modal-title").html("新增线资源信息");

});

function objToform(obj) {
	for(var attr in obj) {
		if($("#" + attr).length > 0) {
			$("#" + attr).val(obj[attr]);
		}
	}
}

$("#update-btn").click(function() {
	flag = "update";
	$(".modal-title").html("修改线资源信息");
	if(xianJqGridRowData == null || xianJqGridRowData == '') {
		layer.confirm('请指定修改的信息列！', {
			btn: ['是的'] //按钮
		});
		$("#update-btn").attr("data-target", "");
	} else {
		$("#update-btn").attr("data-target", "#myModal");

		//回显
		objToform(xianJqGridRowData);
		
		$("#stationId").find("option[value='" + xianJqGridRowData.stationId + "']").prop("selected", true);
		$("#resourceTypeCode").prop("disabled", true);
		$(".chosen-select").trigger("chosen:updated");
	}

});

$("#delete-btn").click(function() {
	if(xianJqGridRowData == null || xianJqGridRowData == '') {
		layer.confirm('请指定修改的信息列！', {
			btn: ['是的'] //按钮
		});

	} else {
		layer.confirm('是否确定要删除？', {
			btn: ['是的', '取消'] //按钮
		}, function() {
			layer.closeAll('dialog');
			$.ajax({
				type: "delete",
                url: YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/MapLine/deleteMapLine/" + xianJqGridRowData.id,
				dataType: "text",
				async: false,
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					//console.log(data);
					if(data == 500 || data == '500') {
						layer.msg('删除失败', {
							icon: 2
						});

					} else {
						layer.msg('删除成功', {
							icon: 1
						});
						setTimeout(function() {
							location.reload();
						}, 1000);

					}

				},
				error: function(returndata) {
					//console.log(returndata);
					layer.msg('删除失败', {
						icon: 2
					});

				}

			});
		});
	}
});


//搜索按钮点击事件
$("#plsCarTpSch").click(function() {
	if($("#namename").val().trim() === "" && $("#linecodelinecode").val().trim() === "" && $("#stationidstationid").val().trim() === ""){
		$.jqPaginator('#pagination1', {
			totalPages: totalPage,
			visiblePages: 10,
			currentPage: 1,
			onPageChange: function(num, type) {

				var mydata;
				$.ajax({
					type: "get",
					url: YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/MapLine/",
					data: {
						"pageIndex": num - 1,
						"pageSize": 10,

					},
					dataType: "json",
					async: false,
					headers: {
						"Content-Type": "application/json;charset=UTF-8"
					},
					success: function(data) {
						//	console.log(data);
						mydata = data.data;
						totalRecords = data.elementsSum;
						var length = mydata.length;
			var listArray = [];
			for(var i = 0; i < length; i++) {
				listArray.push(mydata[i]['stationid']);
			}
			//console.log("listArray"+JSON.stringify(listArray));
			$.ajax({
				type: "post",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/byIdList",
				dataType: "json",
				data: JSON.stringify(listArray),
				async: false,
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					var lengthOne = data.length;
					for(var j = 0; j < lengthOne; j++) {
						mydata[j]['stationName'] = data[j]['stationName'];
					}
					//console.log("data"+JSON.stringify(data));
					//console.log("mydata"+JSON.stringify(mydata));

				},
				error: function(returndata) {
					console.log(returndata);

				}

			});
					}
				});
                $("#stationidstationid option").eq(0).prop("selected", true);
				$("#stationidstationid").trigger("chosen:updated");
				$("#table_list_1").jqGrid('clearGridData'); //清空表格
				$("#table_list_1").jqGrid('setGridParam', { // 重新加载数据
					datatype: 'local',
					data: mydata, //  newdata 是符合格式要求的需要重新加载的数据 

				}).trigger("reloadGrid");
			}
		});
	}else{
		var array = ["name","linecode","stationid"];
		var yzurl = likeQuery(YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/MapLine/?", array);
//		console.log(url);
		$.jqPaginator('#pagination1', {
			totalPages: totalPage,
			visiblePages: 10,
			currentPage: 1,
			onPageChange: function(num, type) {

				var mydata;
				$.ajax({
					type: "get",
					url: yzurl,
					data: {
						"pageIndex": num - 1,
						"pageSize": 10,

					},
					dataType: "json",
					async: false,
					headers: {
						"Content-Type": "application/json;charset=UTF-8"
					},
					success: function(data) {
						//	console.log(data);
						mydata = data.data;
						totalRecords = data.elementsSum;
						var length = mydata.length;
			var listArray = [];
			for(var i = 0; i < length; i++) {
				listArray.push(mydata[i]['stationid']);
			}
			//console.log("listArray"+JSON.stringify(listArray));
			$.ajax({
				type: "post",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/byIdList",
				dataType: "json",
				data: JSON.stringify(listArray),
				async: false,
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					var lengthOne = data.length;
					for(var j = 0; j < lengthOne; j++) {
						mydata[j]['stationName'] = data[j]['stationName'];
					}
					//console.log("data"+JSON.stringify(data));
					//console.log("mydata"+JSON.stringify(mydata));

				},
				error: function(returndata) {
					console.log(returndata);

				}

			});
					}
				});
                $("#stationidstationid option").eq(0).prop("selected", true);
				$("#stationidstationid").trigger("chosen:updated");
				$("#table_list_1").jqGrid('clearGridData'); //清空表格
				$("#table_list_1").jqGrid('setGridParam', { // 重新加载数据
					datatype: 'local',
					data: mydata, //  newdata 是符合格式要求的需要重新加载的数据 

				}).trigger("reloadGrid");
			}
		});
	}
});

//表单提交

function xianAction() {

	if(flag == "add") {
		//console.log(JSON.stringify($('#commentForm').serializeJSON()));
		$.ajax({
			type: "post",
			url: YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/MapLine/postMapLine",
			data: JSON.stringify($('#commentForm').serializeJSON()),
			dataType: "text",
			async: false,
			headers: {
				"Content-Type": "application/json;charset=UTF-8"
			},
			success: function(data) {
				if(data == 500 || data == '500') {
					layer.msg('新增失败', {
						icon: 2
					});
					$('#myModal').modal('show');
				} else {
					layer.msg('新增成功', {
						icon: 1
					});

					$('#myModal').modal('hide');
					setTimeout(function() {
						location.reload();
					}, 1000);

				}
			},

			error: function(returndata) {
				console.log(returndata);

				layer.msg('新增失败', {
					icon: 2
				});
				$('#myModal').modal('show');
			}
		});

	} else if(flag == "update") {

		$.ajax({
			type: "put",
			url: YZ.ajaxURLms+"api/jp-BIRM-Resource-ms/MapLine/putMapLine/" + xianJqGridRowData.id,
			data: JSON.stringify($('#commentForm').serializeJSON()),
			dataType: "text",
			async: false,
			headers: {
				"Content-Type": "application/json;charset=UTF-8"
			},
			success: function(data) {
				if(data == 500 || data == '500') {
					layer.msg('修改失败', {
						icon: 2
					});
					$('#myModal').modal('show');
				} else {
					layer.msg('修改成功', {
						icon: 1
					});
					$('#myModal').modal('hide');
					setTimeout(function() {
						location.reload();
					}, 1000);
				}
			},

			error: function(returndata) {

				layer.msg('修改失败', {
					icon: 2
				});
				$('#myModal').modal('show');
			}
		});

	}
}