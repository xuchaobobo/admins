
$(document).ready(function() {
	
	$.jgrid.defaults.styleUI = 'Bootstrap';
$('#date1').datetimepicker({　　
	  
	　　format: "yyyy-mm-dd",
	　　autoclose: true
});

	$(".chosen-select").chosen();

	//树结构节点nodes
	// var treeViewNodeData;
	var roles = [];
	var roleIds = [];
	var userid=window.sessionStorage.userId;			
	var mydata;
	var deptname;

	var deptId;
	var deptno;
	var setting = {
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			beforeClick: function(treeId, treeNode) {
				deptname = treeNode.name;
				deptId = treeNode.id;
				$.ajax({
					type: "get",
					url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/users/userInfo/stationId/" + treeNode.id,
					//data: null,
					dataType: "json",
					//async: false,
					headers: {
						"Content-Type": "application/json;charset=UTF-8"
					},
					success: function(data) {
						mydata=data
						$("#table_list_1").jqGrid('clearGridData'); //清空表格
						$("#table_list_1").jqGrid('setGridParam', { // 重新加载数据
							datatype: 'local',
							data: mydata, //  newdata 是符合格式要求的需要重新加载的数据 
							page: 1
						}).trigger("reloadGrid");
					}
				});
				//deptno=treeNode.id;
				//console.log(treeNode.id + "  " + treeNode.name);
			}
		}
	};

	var allzNodes;

	$.ajax({
		type: "get",
		url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/getOrgaInfo",
		//data: null,
		dataType: "json",
		async: false,
		headers: {
			"Content-Type": "application/json;charset=UTF-8"
		},
		success: function(data) {
			//console.log(data);
			allzNodes = data;

			for(var i = 0; i < data.length; i++) {
				var ele = new Option(data[i].name, data[i].id);
				$("#stationId1").append(ele);
			}

		}
	});

	//console.log(YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/"+treeNode.id);
	/*$.ajax({
		type: "get",
		url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/" + treeNode.id,
		//data: null,
		dataType: "json",
		//async: false,
		headers: {
			"Content-Type": "application/json;charset=UTF-8"
		},
		success: function(data) {
			console.log(data);
		}
	});*/

	$.fn.zTree.init($("#tree-dep"), setting, allzNodes);

	function init3Tree() {

		var myDiv = $(".parent-div-dept");
		$(".stationName").click(function(event) {

			if(!$(this).attr("readonly")) {
				$(myDiv).fadeIn(); //调用显示DIV方法
				$(document).one("click", function() { //对document绑定一个影藏Div方法
					$(myDiv).hide();
				});

				event.stopPropagation(); //阻止事件向上冒泡
			}
		});
		$(myDiv).click(function(event) {
			event.stopPropagation(); //阻止事件向上冒泡
		});

		var myDiv2 = $(".parent-div-type");
		$(".policeTypeName").click(function(event) {
			$(myDiv2).fadeIn(); //调用显示DIV方法

			$(document).one("click", function() { //对document绑定一个影藏Div方法
				$(myDiv2).hide();
			});

			event.stopPropagation(); //阻止事件向上冒泡
		});
		$(myDiv2).click(function(event) {
			event.stopPropagation(); //阻止事件向上冒泡
		});

		var myDiv1 = $(".parent-div-job");
		$(".policePositionName").click(function(event) {
			$(myDiv1).fadeIn(); //调用显示DIV方法

			$(document).one("click", function() { //对document绑定一个影藏Div方法
				$(myDiv1).hide();
			});

			event.stopPropagation(); //阻止事件向上冒泡
		});
		$(myDiv1).click(function(event) {
			event.stopPropagation(); //阻止事件向上冒泡
		});

	}
	init3Tree();

	var searchText;
	$("#deptSearch").click(function() {
		searchText = $("#searchText").val();
		if(searchText != null && searchText != '') {
			var zNodes;
			$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/getStationInfoByStationNameLike/" + searchText,
				//data: null,
				dataType: "json",
				async: false,
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					var nodes = [];
					for(var i = 0; i < data.length; i++) {
						var node = {
							id: data[i].id,
							name: data[i].stationName,
							pId: data[i].parentId
						};
						nodes.push(node);
					}
					zNodes = nodes;
				}
			});
			$.fn.zTree.init($("#tree-dep"), setting, zNodes);
			searchText = '';
		} else {

			$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/policestations/getOrgaInfo",
				//data: null,
				dataType: "json",
				//async: false,
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					//console.log(data);
					allzNodes = data;
				}
			});
			$.fn.zTree.init($("#tree-dep"), setting, allzNodes);
		}

	});
/*$.jqPaginator('#pagination1', {
        totalPages: 100,
        visiblePages: 10,
        currentPage: 1,
        onPageChange: function (num, type) {
          
        }
	});*/
	
	var userJqGridRowData;
	// Configuration for jqGrid Example 1
	//9BECB65D-4E52-4039-993B-369DAA145EAF
	var userid=window.sessionStorage.userId;
	$.ajax({
		type: "get",
		url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/byUserId/"+userid,
		//data: null,
		dataType: "json",
		async: false,
		headers: {
			"Content-Type": "application/json;charset=UTF-8"
		},
		success: function(data) {
			console.log(data);
			mydata = data;
		}
	});
	
	$("#table_list_1").jqGrid({
		data: mydata,
		datatype: "local",
		height: 530,
		autowidth: true,
		shrinkToFit: true,
		rowNum: 10,
		//rownumbers: true,
		rowList: [10, 20, 30],
		colNames: ['姓名', '所属单位', '警号', '用户名', '账号', '办公电话', '当前状态','操作'],
		colModel: [{
				name: 'id',
				index: 'id',
				hidden: true
			},
			{
				name: 'policeStationInfo.stationName',
				index: 'policeStationInfo.stationName',
				width: 100
			},
			{
				name: 'policeCode',
				index: 'policeCode',
				width: 90,

			},
			{
				name: 'policeName',
				index: 'policeName',
				width: 100
			},
			{
				name: 'loginName',
				index: 'loginName',
				width: 80,

			},
			{
				name: 'officeTel',
				index: 'officeTel',
				width: 80,

			},
			{
				name: 'status',
				index: 'status',
				width: 80,
				align: "left",
				sorttype: "float",
			},

			{
				name: 'id',
				index: 'id',
				width: 80,
				align:'center',
				hidden: true
			}
		],
		pager: "#pager_list_1",
		subGrid : true,//子表格启用
		subGridOptions : {//子表格选项
		plusicon : "ace-icon fa fa-plus center bigger-110 blue",//展开图标
		minusicon : "ace-icon fa fa-minus center bigger-110 blue",//收缩图标
		openicon : "ace-icon fa fa-chevron-right center orange",//打开时左侧图标
		// expandOnLoad: true,
		},
		//子表格展开时
		subGridRowExpanded: function(subgrid_id, row_id) {
			var subgrid_table_id;
			var rowObject=$("#table_list_1").jqGrid('getRowData',row_id);
			var rowdata=mydata.filter(item=>{
				if(item.loginName==rowObject.loginName){
					return item;
				}
			})[0];
			
			var mount=initMenu(rowdata.policeId);
			console.log(mount);
			if(mount.RADIO){
				rowdata.radio=mount.RADIO
			}else{
				rowdata.radio=''
			}
			if(mount.LOGGER){
				rowdata.logger=mount.LOGGER
			}else{
				rowdata.logger=''
			}
			if(mount.APP){
				rowdata.app=mount.APP
			}else{
				rowdata.app=''
			}
			if(mount.CAR){
				rowdata.car=mount.CAR
			}else{
				rowdata.car=''
			}
			if(mount.MONITOR){
				rowdata.monitor=mount.MONITOR
			}else{
				rowdata.monitor=''
			}
			console.log(rowdata);
			if(!mount.status){
				mount.status=0;
			}
			if(!rowdata.status==undefined){
				rowdata.status=0;
			}
			subgrid_table_id = subgrid_id+"_t";
			$("#"+subgrid_id).html(htmltbale);
			
			var htmltbale= `<table id=${subgrid_table_id} class='scroll' style="width:100%">
				<tr>
					<td width='30%'>用户状态：${rowdata.status==1?"启用":"禁用"}</td><td width='30%'>生日：${rowdata.birthday==null?"":dateFtt('yyyy-MM-dd',rowdata.birthday)}</td><td>手台：${rowdata.radio==null?"":rowdata.radio}</td>
				</tr>
				<tr>
					<td>密码：${rowdata.pwd}</td><td>排序：${rowdata.policeType.sort==null?"":rowdata.policeType.sort}</td><td>执法记录仪：${rowdata.logger==null?"":rowdata.logger}</td>
				</tr>
				<tr>
					<td>性别：${rowdata.gender==null?"":rowdata.gender}</td><td>逻辑删除：${rowdata.state==null?"":rowdata.state}</td><td>警务通：${rowdata.app==null?"":rowdata.app}</td>
				</tr>
				<tr>
					<td>经纬度：经度${rowdata.policeLatitude==null?"":rowdata.policeLatitude} 纬度${rowdata.policeLongitude==null?"":rowdata.policeLongitude}</td><td>是否测试员：${rowdata.state}</td><td>警车：${rowdata.car==null?"":rowdata.car}</td>
				</tr>
				<tr>
					<td>警员类型：${rowdata.policeType.type==null?"":rowdata.policeType.type}</td><td>部门相片${rowdata.pictureUrl==null?"":rowdata.pictureUrl}</td><td>单兵：${rowdata.monitor==null?"":rowdata.monitor}</td>
				</tr>

			</table>`
			$("#"+subgrid_id).html(htmltbale);
		},
		viewrecords: true,
		caption: "",
		hidegrid: false,
		onSelectRow: function(rowId, status) {
			let rowObject = $("#table_list_1").jqGrid("getRowData", rowId);
			userJqGridRowData=mydata.filter(item=>{
				if(item.loginName==rowObject.loginName){
					return item;
				}
			})[0];
			
			let mount=initMenu(userJqGridRowData.policeId);
			console.log(mount);
			if(mount.RADIO){
				userJqGridRowData.radio=mount.RADIO
			}else{
				userJqGridRowData.radio=''
			}
			if(mount.LOGGER){
				userJqGridRowData.logger=mount.LOGGER
			}else{
				userJqGridRowData.logger=''
			}
			if(mount.APP){
				userJqGridRowData.app=mount.APP
			}else{
				userJqGridRowData.app=''
			}
			if(mount.CAR){
				userJqGridRowData.car=mount.CAR
			}else{
				userJqGridRowData.car=''
			}
			if(mount.MONITOR){
				userJqGridRowData.monitor=mount.MONITOR
			}else{
				userJqGridRowData.monitor=''
			}
			//console.log(userJqGridRowData);
		},
		onPaging:function(pageBtn){  
        var re_page = $('#table_list_1').getGridParam('page');//获取返回的当前页          
        var re_total= $('#table_list_1').getGridParam('lastpage');//获取总页数  
     
        //console.log("总页数："+re_total);
         
          if(pageBtn=="next"){  
           // console.log("当前页："+(re_page+1));
          }
            if(pageBtn=="prev"){  
           // console.log("当前页："+(re_page-1));
            }  
            if(pageBtn=="first"){  
            //  console.log("当前页："+"1");
            }  
            if(pageBtn=="last"){  
             // console.log("当前页："+re_total);  
            }  
            if(pageBtn=="user"){  
              var page = $(".ui-pg-input").val();
            //  console.log("当前页："+page);
            }  
          
        } 
	});
	//$("#table_list_1").setSelection(1, true);

	// Add responsive to jqGrid
	$(window).bind('resize', function() {
		var width = $('.jqGrid_wrapper').width();
		$('#table_list_1').setGridWidth(width);

	});

	//时间控件
	$('#date').datetimepicker({　　
		minView: "month",
		　　format: "yyyy-mm-dd",
		　　autoclose: true
	});

	$('#date1').datetimepicker({　　
		minView: "month",
		　　format: "yyyy-mm-dd",
		　　autoclose: true
	});

	$("#searchUser").click(function() {
		var searchUserText = $("#searchUserText").val();

		if(deptId == null || deptId == '') {
			deptId = '1';
		}

		$.ajax({
			type: "get",
			url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/stationId/" + deptId,
			data: {
				userName: searchUserText
			},
			dataType: "json",
			headers: {
				"Content-Type": "application/json;charset=UTF-8"
			},
			success: function(data) {

				$("#table_list_1").jqGrid('clearGridData'); //清空表格
				$("#table_list_1").jqGrid('setGridParam', { // 重新加载数据
					datatype: 'local',
					data: data, //  newdata 是符合格式要求的需要重新加载的数据 
					page: 1
				}).trigger("reloadGrid");

			}
		});

	});

	$("#update-btn").click(function() {
		console.log(userJqGridRowData);
		if(userJqGridRowData == null || userJqGridRowData == '') {
			layer.confirm('请指定修改的信息列！', {
				btn: ['是的'] //按钮
			});
			$("#update-btn").attr("data-target", "");
		} else {
			$("#update-btn").attr("data-target", "#myModalUpdate");
			//数据回显过程

			// $("#username").val(userJqGridRowData.username);
			// $("#loginName").val(userJqGridRowData.loginName);

			// $("#pwd").val(userJqGridRowData.pwd);
			$("#pictureUrl").attr("readonly","readonly");

			$("#name").val(userJqGridRowData.policeName);
			$("#policeCode").val(userJqGridRowData.policeCode);
            /* console.log(policeJqGridRowData.gender);*/
			if(userJqGridRowData.gender.match('男')) {
				$("#gender").find("option[value='女']").removeAttr("selected");
				$("#gender").find("option[value='男']").attr("selected", "selected");
				$("#gender").find("option[value='男']").prop("selected", true);
			}
			if(userJqGridRowData.gender.match('女')){
				$("#gender").find("option[value='男']").removeAttr("selected");
				$("#gender").find("option[value='女']").attr("selected", "selected");
				$("#gender").find("option[value='女']").prop("selected", true);
			}

			$("#stationId").val(userJqGridRowData.stationId);
			$("#pictureUrl").val(userJqGridRowData.pictureUrl);
			$("#telephone").val(userJqGridRowData.telephone);
			$("#longitude").val(userJqGridRowData.policeLongitude);
			$("#latitude").val(userJqGridRowData.policeLatitude);
			$("#status").val(userJqGridRowData.status);
			if(userJqGridRowData.status == 1 || userJqGridRowData.status == '1') {
				$("#status").find("option[value='1']").attr("selected", "selected");
				$("#status").find("option[value='1']").prop("selected", true);
			} else {
				$("#status").find("option[value='0']").attr("selected", "selected");
				$("#status").find("option[value='0']").prop("selected", true);
			}

			$("#policeTypeId").val(userJqGridRowData.policeType.id);
			$("#policeTypeName").val(userJqGridRowData.policeType.type);
			$("#type").val(userJqGridRowData.policeType.type);
			$("#stationId").val(userJqGridRowData.policeStationInfo.id);
			$("#stationName").val(userJqGridRowData.policeStationInfo.stationName);
			$("#policePosition").find("option[value='"+userJqGridRowData.policePosition.id+"']").attr("selected", "selected");
			


			$("#policePosition").val(userJqGridRowData.policePosition.id);
			$("#policePositionName").val(userJqGridRowData.policePosition.position);
			$("#officeTel").val(userJqGridRowData.officeTel);
			$("#datetimepicker").val(userJqGridRowData.birthday);
			$("#sort").val(userJqGridRowData.sort);
			$("#portable").val(userJqGridRowData.radio);
			$("#policing").val(userJqGridRowData.app);
			$("#policeCar").val(userJqGridRowData.car);
			$("#singleSoldier").val(userJqGridRowData.monitor);
			$("#siteRecode").val(userJqGridRowData.logger);
			if(userJqGridRowData.isDelete == 1 || userJqGridRowData.isDelete == '1') {
				$("#isDelete1").attr("checked", "checked");
			} else {
				$("#isDelete0").attr("checked", "checked");
			}
			if(userJqGridRowData.isTestPolice == 1 || userJqGridRowData.isTestPolice == '1') {
				$("#isTestPolice1").attr("checked", "checked");
			} else {
				$("#isTestPolice0").attr("checked", "checked");
			}
			$(".chosen-select").trigger("chosen:updated");
			var setting1 = {
				data: {
					simpleData: {
						enable: true
					}
				},
				callback: {
					beforeClick: function(treeId, treeNode) {

						deptname = treeNode.name;
						$("#stationName").val(deptname);
						$("#stationId").val(treeNode.stationId);

					}
				}
			};

			$.fn.zTree.init($("#tree-parent-dept"), setting1, allzNodes);

			var jobs = [];
			$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/fetchAllPolicePosition",

				dataType: "json",

				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {

					for(var i = 0; i < data.length; i++) {
						var job = {
							id: data[i].id,
							name: data[i].position
						}
						jobs.push(job);
					}

					var settingjob = {
						data: {
							simpleData: {
								enable: true
							}
						},
						callback: {
							beforeClick: function(treeId, treeNode) {
								deptname = treeNode.name;
								
								$("#policePosition").val(treeNode.id);
								$("#policePositionName").val(treeNode.name);

							}
						}
					};

					$.fn.zTree.init($("#tree-parent-job"), settingjob, jobs);
				}
			});

			var types = [];
			$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/fetchAllPoliceType",

				dataType: "json",

				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {

					for(var i = 0; i < data.length; i++) {
						var type = {
							id: data[i].id,
							name: data[i].type
						}
						types.push(type);
					}

					var settingtype = {
						data: {
							simpleData: {
								enable: true
							}
						},
						callback: {
							beforeClick: function(treeId, treeNode) {
								deptname = treeNode.name;
								
								$("#policeTypeId").val(treeNode.id);
								$("#policeTypeName").val(treeNode.name);

							}
						}
					};

					$.fn.zTree.init($("#tree-parent-type"), settingtype, types);
				}
			});
			if(userJqGridRowData.state == 1 || userJqGridRowData.state == '1') {
				$("#state").find("option[value='1']").attr("selected", "selected");
				$("#state").find("option[value='1']").prop("selected", true);
			} else {
				$("#state").find("option[value='0']").attr("selected", "selected");
				$("#state").find("option[value='0']").prop("selected", true);
			}

		}

	});

	$("#updateUserBtn").click(function() {
		$("#update-btn").attr("data-target", "");
		var commentFormUpdate = $('#commentForm').serializeJSON();
		console.log(commentFormUpdate);
		if(!commentFormUpdate.birthday.match("T00:00:00.0000000+08:00")){
			commentFormUpdate.birthday = commentFormUpdate.birthday + "T00:00:00.0000000+08:00";
		}	
		if(commentFormUpdate.birthday !=' T00:00:00.0000000+08:00'){			
		if(commentFormUpdate.policeCode != null && commentFormUpdate.policeCode != '' &&
		commentFormUpdate.name!=null && commentFormUpdate.name!=''&&
		commentFormUpdate.stationId!=null && commentFormUpdate.stationId!=''&&
		commentForm.telephone!=null && commentFormUpdate.telephone!='' &&
		commentFormUpdate.policeTypeId!=null && commentFormUpdate.policeTypeId!=''&&
		commentFormUpdate.policePosition!=null && commentFormUpdate.policePosition!='' 
		) {
			$.ajax({
				type: "put",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/" + userJqGridRowData.policeId,
				data: JSON.stringify(commentFormUpdate),
				dataType: "json",
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {

					if(data == 500 || data == '500') {
						layer.msg('修改失败', {
							icon: 1
						});
					} else {
						var formdata=$('#editeqForm').serializeJSON();
						console.log(formdata,userJqGridRowData.policeId);
						let arrs=[];
						arrs.push(new CreateObj('POLICE',userJqGridRowData.policeId,'RADIO',formdata.RADIO))
				
						arrs.push(new CreateObj('POLICE',userJqGridRowData.policeId,'LOGGER',formdata.LOGGER))
					
						arrs.push(new CreateObj('POLICE',userJqGridRowData.policeId,'APP',formdata.APP))
					
					
						arrs.push(new CreateObj('POLICE',userJqGridRowData.policeId,'MONITOR',formdata.MONITOR))
					
						arrs.push(new CreateObj('POLICE',userJqGridRowData.policeId,'CAR',formdata.CAR))
					
						console.log(JSON.stringify(arrs));
						$.ajax({
							type: "post",
							url: YZ.ajaxURLms+"api/jp-DA-CalendarExt-business-ms/facilityRelation/updateRelationship",

							dataType: "json",
							data:JSON.stringify(arrs),
							//async: false,
							headers: {
								"Content-Type": "application/json;charset=UTF-8"
							},
							success: function(data) {
								console.log(data);
								layer.msg('修改成功', {
									icon: 1
								});
								$.ajax({
									type: "get",
									url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/byUserId/"+userid,
		
									dataType: "json",
									//async: false,
									headers: {
										"Content-Type": "application/json;charset=UTF-8"
									},
									success: function(data) {
										mydata=data;
										$("#table_list_1").jqGrid('clearGridData'); //清空表格
										$("#table_list_1").jqGrid('setGridParam', { // 重新加载数据
											datatype: 'local',
											data: mydata, //  newdata 是符合格式要求的需要重新加载的数据 
											page: 1
										}).trigger("reloadGrid");
										userJqGridRowData=null;
									},
		
								});
							},

						});
						
					}
				},
				error: function(returndata) {

					layer.msg('修改失败', {
						icon: 1
					});
					$("#pictureUrl").attr("readonly","readonly");

			$("#name").val(userJqGridRowData.policeName);
			$("#policeCode").val(userJqGridRowData.policeCode);
            /* console.log(policeJqGridRowData.gender);*/
			if(userJqGridRowData.gender.match('男')) {
				$("#gender").find("option[value='女']").removeAttr("selected");
				$("#gender").find("option[value='男']").attr("selected", "selected");
				$("#gender").find("option[value='男']").prop("selected", true);
			}
			if(userJqGridRowData.gender.match('女')){
				$("#gender").find("option[value='男']").removeAttr("selected");
				$("#gender").find("option[value='女']").attr("selected", "selected");
				$("#gender").find("option[value='女']").prop("selected", true);
			}

			$("#stationId").val(userJqGridRowData.stationId);
			$("#pictureUrl").val(userJqGridRowData.pictureUrl);
			$("#telephone").val(userJqGridRowData.telephone);
			$("#longitude").val(userJqGridRowData.policeLongitude);
			$("#latitude").val(userJqGridRowData.policeLatitude);
			$("#status").val(userJqGridRowData.status);
			if(userJqGridRowData.status == 1 || userJqGridRowData.status == '1') {
				$("#status").find("option[value='1']").attr("selected", "selected");
				$("#status").find("option[value='1']").prop("selected", true);
			} else {
				$("#status").find("option[value='0']").attr("selected", "selected");
				$("#status").find("option[value='0']").prop("selected", true);
			}

			$("#policeTypeId").val(userJqGridRowData.policeType.id);
			$("#policeTypeName").val(userJqGridRowData.policeType.type);
			$("#type").val(userJqGridRowData.policeType.type);
			$("#stationId").val(userJqGridRowData.policeStationInfo.id);
			$("#stationName").val(userJqGridRowData.policeStationInfo.stationName);
			$("#policePosition").find("option[value='"+userJqGridRowData.policePosition.id+"']").attr("selected", "selected");
			


			$("#policePosition").val(userJqGridRowData.policePosition.id);
			$("#policePositionName").val(userJqGridRowData.policePosition.position);
			$("#officeTel").val(userJqGridRowData.officeTel);
			$("#datetimepicker").val(userJqGridRowData.birthday);
			$("#sort").val(userJqGridRowData.sort);
			$("#portable").val(userJqGridRowData.radio);
			$("#policing").val(userJqGridRowData.app);
			$("#policeCar").val(userJqGridRowData.car);
			$("#singleSoldier").val(userJqGridRowData.monitor);
			$("#siteRecode").val(userJqGridRowData.logger);
			if(userJqGridRowData.isDelete == 1 || userJqGridRowData.isDelete == '1') {
				$("#isDelete1").attr("checked", "checked");
			} else {
				$("#isDelete0").attr("checked", "checked");
			}
			if(userJqGridRowData.isTestPolice == 1 || userJqGridRowData.isTestPolice == '1') {
				$("#isTestPolice1").attr("checked", "checked");
			} else {
				$("#isTestPolice0").attr("checked", "checked");
			}
			$(".chosen-select").trigger("chosen:updated");
			var setting1 = {
				data: {
					simpleData: {
						enable: true
					}
				},
				callback: {
					beforeClick: function(treeId, treeNode) {

						deptname = treeNode.name;
						$("#stationName").val(deptname);
						$("#stationId").val(treeNode.stationId);

					}
				}
			};

			$.fn.zTree.init($("#tree-parent-dept"), setting1, allzNodes);

			var jobs = [];
			$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/fetchAllPolicePosition",

				dataType: "json",

				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					console.log(data);
					for(var i = 0; i < data.length; i++) {
						var job = {
							id: data[i].id,
							name: data[i].position
						}
						jobs.push(job);
					}

					var settingjob = {
						data: {
							simpleData: {
								enable: true
							}
						},
						callback: {
							beforeClick: function(treeId, treeNode) {
								deptname = treeNode.name;
								
								$("#policePosition").val(treeNode.id);
								$("#policePositionName").val(treeNode.name);

							}
						}
					};

					$.fn.zTree.init($("#tree-parent-job"), settingjob, jobs);
				}
			});

			var types = [];
			$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/fetchAllPoliceType",

				dataType: "json",

				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {

					for(var i = 0; i < data.length; i++) {
						var type = {
							id: data[i].id,
							name: data[i].type
						}
						types.push(type);
					}

					var settingtype = {
						data: {
							simpleData: {
								enable: true
							}
						},
						callback: {
							beforeClick: function(treeId, treeNode) {
								deptname = treeNode.name;
								
								$("#policeTypeId").val(treeNode.id);
								$("#policeTypeName").val(treeNode.name);

							}
						}
					};

					$.fn.zTree.init($("#tree-parent-type"), settingtype, types);
				}
			});
			if(userJqGridRowData.state == 1 || userJqGridRowData.state == '1') {
				$("#state").find("option[value='1']").attr("selected", "selected");
				$("#state").find("option[value='1']").prop("selected", true);
			} else {
				$("#state").find("option[value='0']").attr("selected", "selected");
				$("#state").find("option[value='0']").prop("selected", true);
			}

		
					$('myModalUpdate').modal('show');
				}

			});
		} else {
			
			layer.confirm('信息请填写完整？', {
			btn: ['是的'] //按钮
		}, function() {
			layer.closeAll('dialog');
			$('#myModalUpdate').modal('show');
			});
		
		

	}
	}else{

		layer.confirm('请选择生日日期？', {
			btn: ['是的'] //按钮
		}, function() {
			layer.closeAll('dialog');
			$('#myModalUpdate').modal('show');
			});

	}
	});

	$("#delete-btn").click(function() {
		if(userJqGridRowData == null || userJqGridRowData == '') {
			layer.confirm('请指定删除的信息列！', {
				btn: ['是的'] //按钮
			});

		} else {
			layer.confirm('是否确定要删除？', {
				btn: ['是的', '取消'] //按钮
			}, function() {
				layer.closeAll('dialog');
				$.ajax({
					type: "delete",
					url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/users/deletePoliceInfoAndUser/" + userJqGridRowData.userId,
					dataType: "text",
					headers: {
						"Content-Type": "application/json;charset=UTF-8"
					},
					success: function(data) {
						if(data == 500 && data == '500') {

							layer.msg('删除失败', {
								icon: 1
							});

						} else {
							layer.msg('删除成功', {
								icon: 1
							});
							$.ajax({
								type: "get",
								url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/byUserId/"+userid,
								dataType: "json",
								headers: {
									"Content-Type": "application/json;charset=UTF-8"
								},
								success: function(data) {
									$("#table_list_1").jqGrid('clearGridData'); //清空表格
									$("#table_list_1").jqGrid('setGridParam', { // 重新加载数据
										datatype: 'local',
										data: data, //  newdata 是符合格式要求的需要重新加载的数据 
										page: 1
									}).trigger("reloadGrid");
								}
							});
						}
					},
					error: function(returndata) {

						layer.msg('删除失败', {
							icon: 1
						});
					}
				});

			});
		}
	});

	$("#deleteRole").click(function() {
		if(userJqGridRowData == null || userJqGridRowData == '') {
			layer.confirm('请指定的信息列！', {
				btn: ['是的'] //按钮
			});
			 	$("#deleteRole").attr("data-target", "");
		} else {
      
			var code;
			var changedNodes;
			 $("#deleteRole").attr("data-target", "#myModalDeleteRole");

			function zTreeOnCheck(event, treeId, treeNode) {

				refreshLayers();
				clearCheckedOldNodes();
			};

			var layers;

			function setCheck() {
				var zTree = $.fn.zTree.getZTreeObj("treeDemoDeleteRole"),
					py = $("#py").attr("checked") ? "p" : "",
					sy = $("#sy").attr("checked") ? "s" : "",
					pn = $("#pn").attr("checked") ? "p" : "",
					sn = $("#sn").attr("checked") ? "s" : "",
					type = {
						"Y": py + sy,
						"N": pn + sn
					};
				zTree.setting.check.chkboxType = type;
				showCode('setting.check.chkboxType = { "Y" : "' + type.Y + '", "N" : "' + type.N + '" };');
			}

			function showCode(str) {
				if(!code) code = $("#code");
				code.empty();
				code.append("<li>" + str + "</li>");
			}

			var rolesByUser = [];
			var roleDeleteIds = [];

			//查询用户下的所有角色
			$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/users/fetchByUserId/" + userJqGridRowData.userId,

				dataType: "json",

				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {

					for(var i = 0; i < data.length; i++) {
						var role = {
							id: data[i].id,
							name: data[i].name,
							pId: 0,
						};
						rolesByUser.push(role);

					}

					var settingRole = {
						check: {
							enable: true
						},
						callback: {
							onCheck: function(event, treeId, treeNode) {

								roleDeleteIds.push(treeNode.id);
								var zTree = $.fn.zTree.getZTreeObj("treeDemoDeleteRole");
								changedNodes = zTree.getChangeCheckedNodes();

							}
						},
						data: {
							simpleData: {
								enable: true
							}
						}
					};

					$.fn.zTree.init($("#treeDemoDeleteRole"), settingRole, rolesByUser);
					setCheck();
					$("#py").bind("change", setCheck);
					$("#sy").bind("change", setCheck);
					$("#pn").bind("change", setCheck);
					$("#sn").bind("change", setCheck);
				}
			});

			//delete角色授权

			$("#roleDeleteBtn").click(function() {
				

				$.ajax({
					type: "put",
					
					url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/users/revokeUserRole/" + userJqGridRowData.userId + "?" + "roleIds=" + roleDeleteIds,

					dataType: "json",

					headers: {
						"Content-Type": "application/json;charset=UTF-8"
					},
					success: function(data) {
						if(data == 500 || data == '500') {
							layer.confirm('删除授权失败！', {
								btn: ['是的'] //按钮
							});
						} else {
							layer.msg('删除授权成功!', {
								icon: 1
							});
						}
					},
					error: function(returndata) {

						layer.confirm('删除授权失败！', {
							btn: ['是的'] //按钮
						});
					}
				});
			});
		}

	});

	$("#js-sq").click(function() {
		if(userJqGridRowData == null || userJqGridRowData == '') {
			layer.confirm('请指定的信息列！', {
				btn: ['是的'] //按钮
			});
			$("#js-sq").attr("data-target", "");
		} else {
			$("#js-sq").attr("data-target", "#myModal1");
			var code;
			var changedNodes;
			var layers;
			 roles = [];
			 roleIds = [];
			$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/roles/",

				dataType: "json",

				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {

					for(var i = 0; i < data.length; i++) {
						var role = {
							id: data[i].id,
							name: data[i].name,
							pId: 0,
						};
						roles.push(role);

					}

					var setting = {
						check: {
							enable: true
						},
						callback: {
							onCheck: function(event, treeId, treeNode) {

								roleIds.push(treeNode.id);
							//	console.log(treeNode.id)
								var zTree = $.fn.zTree.getZTreeObj("treeDemo");
							//	changedNodes = zTree.getChangeCheckedNodes();

							}
						},
						data: {
							simpleData: {
								enable: true
							}
						}
					};

					$.fn.zTree.init($("#treeDemo"), setting, roles);
					
				}
			});


		}
	});
	
				//角色授权

			$("#roleUpdateBtn").click(function() {
				$("#js-sq").attr("data-target", "");
				$.ajax({
					type: "put",
					url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/users/giveUserRole/" + userJqGridRowData.userId + "?" + "roleIds=" + roleIds,

					dataType: "text",

					headers: {
						"Content-Type": "application/json;charset=UTF-8"
					},
					success: function(data) {
						if(data == 500 || data == '500') {
							layer.confirm('授权失败！', {
								btn: ['是的'] //按钮
							});
						} else {
							layer.msg('授权成功!', {
								icon: 1
							});
						}
					},
					error: function(returndata) {
						console.log(returndata)
						layer.confirm('授权失败！', {
							btn: ['是的'] //按钮
						});
					}
				});
			});
	$.ajax({
		type: "get",
		url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/fetchAllPoliceType",

		dataType: "json",
		async: false,

		headers: {
			"Content-Type": "application/json;charset=UTF-8"
		},
		success: function(result) {

			for(var i = 0; i < result.length; i++) {
				//new Option(result[i].type, result[i].id);
				var optionEle1 = "<option value='" + result[i].id + "'>" + result[i].type + "</option>";
				$("#policeTypeId1").append(optionEle1);
			}
			$(".chosen-select").trigger("chosen:updated");

		}
	});

    //模板下载
$("#download-modalExcel").click(() => {
	let excelModalDownloadUrl = `${YZ.ajaxURLms}api/jp-BIRM-UserProfile-ms//bs/batch/getTemplate/carInfo`;
	$(location).attr('href', excelModalDownloadUrl);
});

//导入
function doUploadExcel() {
	let formData = new FormData($("#uploadExcel")[0]);
	
	$.ajax({
		url: YZ.ajaxURLms+'api/jp-BIRM-UserProfile-ms//bs/batch/policeInfoAndUser',
		type: 'POST',
		data: formData,
		async: false,
		cache: false,
		contentType: false,
		processData: false,
		success:function(data) {
//					debugger;
//					console.log(data);
					if(data == 500 || data == '500') {
						layer.msg(JSON.parse(data.responseText).errorMsg, {
							icon: 2
						});

					} else {
						layer.msg('导入成功', {
							icon: 1
						});
						setTimeout(function() {
							location.reload();
						}, 1000);

					}

				},
				error: function(returndata) {
					console.log(returndata);
					layer.msg('导入失败', {
						icon: 2
					});

				}
	});
	$('#myModal2').modal('hide');
}

$("#uploadExcelBtn").click(function(){
	doUploadExcel();
});
	$("#add-btn").click(function() {
		//显示弹窗

		$("#addcommentForm").find("input").val("");
		$("#addequipment").find("input").val("");
		$(this).attr("data-target", "#myModal");

		$(".chosen-select").trigger("chosen:updated");

		var jobs = [];
		$.ajax({
			type: "get",
			url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/fetchAllPolicePosition",

			dataType: "json",

			headers: {
				"Content-Type": "application/json;charset=UTF-8"
			},
			success: function(result) {
				for(var i = 0; i < result.length; i++) {
					var optionEle = "<option value='" + result[i].id + "'>" + result[i].position + "</option>";
					$("#policePosition1").append(optionEle);
				}
				$(".chosen-select").trigger("chosen:updated");
			}
		});

		var types = [];
		//警员类型

		function doUpload() {
			var formData = new FormData($("#uploadForm1")[0]);
			$.ajax({
				url: YZ.ajaxURLms+"zuul/api/jp-TIFS-FileCenter-ms/file",
				type: 'POST',
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false,
				success: function(data) {
					var purl = YZ.ajaxURLms+"api/jp-TIFS-FileCenter-ms/file?businessId=" + data.fileID;
					$("#pictureUrl1").val(purl);

					layer.msg('上传成功', {
						icon: 1
					});
				},
				error: function(returndata) {
					layer.msg('上传失败', {
						icon: 1
					});
				}
			});
		}
		$("#uploadBtn1").click(function() {
			doUpload();
		});

	});

	$("#savePoliceBtn").click(function() {

		if($("#loginName1").val().trim()!=""&&$("#username1").val().trim()!=""&&$("#pwd1").val().trim()!=""&&$("#policeCode1").val().trim()!=""&&$("#name1").val().trim()!=""&&$("#telephone1").val().trim()!=""){
		$.ajax({
				type: "get",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/users/bigUserBigModelByloginName/"+$("#loginName1").val(),
				
				dataType: "json",
				async: false,
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					flagLoginName='no';
				//	console.log(data);
				/*	if(data == 500 || data == '500') {
						layer.msg('新增失败', {
							icon: 2
						});
					} else {
						layer.msg('新增成功', {
							icon: 1
						});
				
					}*/
				},

				error: function(returndata) {
				
       		if(returndata.responseJSON.errCode == '404'){
       			flagLoginName='ok';
       			console.log('flagLoginName:'+flagLoginName);
       			
      		 }
				
				}
			});
		
		
		
		
		var addcommentForm = $('#addcommentForm').serializeJSON();
	if(flagLoginName=='ok'){
		var sMobile = $("#telephone1").val().trim(); 
		 if(!(/^1[3|4|5|6|8][0-9]\d{4,8}$/.test(sMobile))){ 
		 	layer.msg('手机号码格式不正确', {
							icon: 2
						});
		  
		  $("#telephone1").focus(); 
		  return false; 
		 } 
		if(addcommentForm.username != null && addcommentForm.username != '' &&
			addcommentForm.loginName != null && addcommentForm.loginName != '' &&
			addcommentForm.pwd != null && addcommentForm.pwd != '' &&
			addcommentForm.policeCode != null && addcommentForm.policeCode != '' &&
			addcommentForm.name != null && addcommentForm.name != '' &&
			addcommentForm.stationId != null && addcommentForm.stationId != '' &&
			
			addcommentForm.policeTypeId != null && addcommentForm.policeTypeId != '' &&
			addcommentForm.policePosition != null && addcommentForm.policePosition != ''
		) {
//if(addcommentForm.telephone.length==11){
			$.ajax({
				type: "post",
				url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/insertPoliceInfoAndUser",
				data: JSON.stringify($('#addcommentForm').serializeJSON()),
				dataType: "text",
				async: false,
				headers: {
					"Content-Type": "application/json;charset=UTF-8"
				},
				success: function(data) {
					if(data == 500 || data == '500') {
						layer.msg('新增失败', {
							icon: 1
						});
					} else {

						console.log(data);
						$.ajax({
							type: "get",
							url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/users/bigUserBigModelByloginName/"+addcommentForm.loginName,

							dataType: "json",
							//async: false,
							headers: {
								"Content-Type": "application/json;charset=UTF-8"
							},
							success: function(data) {
								console.log(data);
								var pliceid=data.policeId;
								let arrs=[];
								//新增设备
								var formdata=$('#addequipment').serializeJSON();
								console.log(formdata,pliceid);
								
								arrs.push(new CreateObj('POLICE',pliceid,'RADIO',formdata.RADIO))
							
								arrs.push(new CreateObj('POLICE',pliceid,'LOGGER',formdata.LOGGER))
							
								arrs.push(new CreateObj('POLICE',pliceid,'APP',formdata.APP))
							
								arrs.push(new CreateObj('POLICE',pliceid,'MONITOR',formdata.MONITOR))
							
								arrs.push(new CreateObj('POLICE',pliceid,'CAR',formdata.CAR))
								
								console.log(JSON.stringify(arrs));
								$.ajax({
									type: "post",
									url: YZ.ajaxURLms+"api/jp-DA-CalendarExt-business-ms/facilityRelation/saveRelationship",

									dataType: "json",
									data:JSON.stringify(arrs),
									//async: false,
									headers: {
										"Content-Type": "application/json;charset=UTF-8"
									},
									success: function(data) {
										if(data == 500 || data == '500') {
											layer.msg('新增失败', {
												icon: 1
											});
										} else {
											console.log(data);
											layer.msg('新增成功', {
												icon: 1
											});

											$.ajax({
												type: "get",
												url: YZ.ajaxURLms+"api/jp-BIRM-UserProfile-ms/polices/byUserId/"+userid,
					
												dataType: "json",
												//async: false,
												headers: {
													"Content-Type": "application/json;charset=UTF-8"
												},
												success: function(data) {
													mydata=data
													$("#table_list_1").jqGrid('clearGridData'); //清空表格
													$("#table_list_1").jqGrid('setGridParam', { // 重新加载数据
														datatype: 'local',
														data: mydata, //  newdata 是符合格式要求的需要重新加载的数据 
														page: 1
													}).trigger("reloadGrid");
												}
											});
									}}
								});
							}
						});
						
						

						

					}
				},

				error: function(returndata) {

					layer.msg('新增失败', {
						icon: 1
					});
					$('#myModal').modal('show');
				}
			});
			/*	}else{
				layer.confirm('请填写正确的电话号码？', {
				btn: ['是的'] //按钮
			}, function() {
				layer.closeAll('dialog');
				$('#myModal').modal('show');
				});
			
		
		}*/
		} else {			
				layer.confirm('信息请填写完整？', {
				btn: ['是的'] //按钮
			}, function() {
				layer.closeAll('dialog');
				$('#myModal').modal('show');
				});

		}
		}else{
		layer.confirm('账号已经存在', {
				btn: ['是的'] //按钮
			}, function() {
				layer.closeAll('dialog');
				$('#myModal').modal('show');
				});
	}
		} else {			
				layer.confirm('信息请填写完整？', {
				btn: ['是的'] //按钮
			}, function() {
				layer.closeAll('dialog');
				$('#myModal').modal('show');
				});

		}

	});

	$(".chosen-select").chosen();

	//点击父级展开树菜单
	　
	var myDiv = $(".parent-div");
	$("#unit").click(function(event) {
		showDiv(); //调用显示DIV方法

		$(document).one("click", function() { //对document绑定一个影藏Div方法
			$(myDiv).hide();
		});

		event.stopPropagation(); //阻止事件向上冒泡
	});
	$(myDiv).click(function(event) {
		event.stopPropagation(); //阻止事件向上冒泡
	});

	function showDiv() {
		$(myDiv).fadeIn();
	}
	
});
function seetable(id){
	console.log(id);
};
function dateFtt(fmt,date)   
{ //author: meizz   
	date=new Date();
  var o = {   
    "M+" : date.getMonth()+1,                 //月份   
    "d+" : date.getDate(),                    //日   
    "h+" : date.getHours(),                   //小时   
    "m+" : date.getMinutes(),                 //分   
    "s+" : date.getSeconds(),                 //秒   
    "q+" : Math.floor((date.getMonth()+3)/3), //季度   
    "S"  : date.getMilliseconds()             //毫秒   
  };   
  if(/(y+)/.test(fmt))   
    fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));   
  for(var k in o)   
    if(new RegExp("("+ k +")").test(fmt))   
  fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
  return fmt;   
} 
function initMenu(id){
	let moution;	
	$.ajax({
		type: "get",
		url: YZ.ajaxURLms+"api/jp-DA-CalendarExt-business-ms/facilityRelation/getRelationship?resourceCode=POLICE&resourceId="+id,

		dataType: "json",
		async: false,
		headers: {
			"Content-Type": "application/json;charset=UTF-8"
		},
		success: function(data) {
			
			if(data){
				moution=data;
			}
			console.log(moution);
			daPoliceRadio = data.daPoliceRadio; //手台
			policeDanbing = data.policeDanbing; //单兵
			policeJwt = data.policeJwt; //警务通
			policeCar = data.policeCar; //警车
			policeUav = data.policeUav; //无人机

		}
	});
	let obj={}
	if(moution!=null){
		moution.map(item=>{
			if(item.targetEntryCode=="RADIO"){
				obj.RADIO=item.targetId
			}else if(item.targetEntryCode=="LOGGER"){
				obj.LOGGER=item.targetId
			}else if(item.targetEntryCode=="APP"){
				obj.APP=item.targetId
			}else if(item.targetEntryCode=="CAR"){
				obj.CAR=item.targetId
			}else if(item.targetEntryCode=="MONITOR"){
				obj.MONITOR=item.targetId
			}
		})
	}
	
	return obj;
}
function CreateObj(code,id,entrycode,targetId){
	this.resourceEntryCode=code;
	this.resourceId=id;
	this.targetEntryCode=entrycode;
	this.targetId=targetId
}