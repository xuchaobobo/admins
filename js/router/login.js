var baseUrl = 'http://172.17.99.10:10014/';

function loginAction() {
	let loginname = $('#commentForm').serializeJSON().loginname;
	$.ajax({
		type: "post",
		url: baseUrl + "api/jp-BIRM-UserProfile-ms//users/login",
		data: JSON.stringify($('#commentForm').serializeJSON()),
		dataType: "text",
		async: false,
		headers: {
			"Content-Type": "application/json;charset=UTF-8"
		},
		success: function(data) {
			console.log(data)
			if(data == true || data == 'true') {
				
				//get userid by loginname
				$.ajax({
					type: "get",
					url: baseUrl + "api/jp-BIRM-UserProfile-ms//users/bigUserBigModelByloginName/" + loginname,
					dataType: "json",
					async: false,
					headers: {
						"Content-Type": "application/json;charset=UTF-8"
					},
					success: function(data) {
						console.log(data)
                        let userId = data.userId;
                       
                        //get page grant by userId
						$.ajax({
							type: "get",
							url: baseUrl + "api/jp-BIRM-UserProfile-ms//opt/function/user/" + userId,
							dataType: "json",
							async: false,
							headers: {
								"Content-Type": "application/json;charset=UTF-8"
							},
							success: function(data) {
								console.log(data)
								let length = data.length;
								let bsPageGrants = [];
								let flagNum = 0;
								for(let i = 0; i<length; i++){
									if(data[i].displayname.match('BS')){
										bsPageGrants.push(data[i]);
									}
								}
								let pageGrantsLength = bsPageGrants.length;
								let html='';
								let navHtml = '';
								let iframeHtml='';
								for(let i=0;i<pageGrantsLength;i++){
									
									html += `<li>
									<a href="#">
								<i class="fa fa-info"></i>
								<span class="nav-label">${bsPageGrants[i].name}</span>
								<span class="fa arrow"></span>
							    </a>
							    <ul class="nav nav-second-level">
							    `;
									bsPageGrants[i].children.forEach(function(item, index){
										let pageName = item.displayname.substring(13);										
										let pageUrl = 'index_'+pageName+'.html';
										if(flagNum == 0){
										navHtml = `<a href="javascript:;" class="active J_menuTab" data-id="${pageUrl}">${item.name}</a>`;
									    iframeHtml = `<iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="${pageUrl}" frameborder="0" data-id="${pageUrl}" seamless></iframe>`;  
									    flagNum++;
										}
										html += `<li>
									<a class="J_menuItem" href="${pageUrl}">${item.name}</a>
								</li>
								`;
									})
									html += `</ul>
						        </li>
						        `;
								}
								
								sessionStorage.setItem("html",html);
								sessionStorage.setItem("loginname",loginname);
								sessionStorage.setItem("navHtml",navHtml);
                                sessionStorage.setItem("iframeHtml",iframeHtml);
                                sessionStorage.setItem("userId",userId);
								$(location).attr('href', 'index.html');
							}
						});

					}
				});

			} else {
				layer.msg('登录失败！', {
					icon: 0
				});

			}
		},

		error: function(returndata) {
			console.log(returndata);

			layer.msg('登录失败！', {
				icon: 0
			});
		}
	});

}
